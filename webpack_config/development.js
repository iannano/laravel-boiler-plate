"use strict";
const webpack           = require("webpack");
const path              = require("path");
const htmlWebpackPlugin = require("html-webpack-plugin");
const extractTextPlugin = require("extract-text-webpack-plugin");

const config = (env) => {
    return {
        entry: {
            vendor: "./resources/assets/app-vendor.js",
            auth: "./resources/assets/app-auth.js",
            entry: "./resources/assets/app-entry.js"
        },
        output: {
            path: path.resolve(__dirname,'../public/assets/'),
            filename: '[name].js',
            publicPath: 'assets/'
        },
        module:{
            rules:[
                {
                    test: /\.css$/,
                    use: extractTextPlugin.extract({
                        fallback: "style-loader",
                        use: [
                            "css-loader"
                        ],
                        publicPath: './'
                    })
                },
                {
                    test: /\.(eot|ttf|woff|woff2|otf)$/,
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: "fonts/[name].[ext]"
                    }
                },
                {
                    test: /\.(jpe?g|png|gif)$/i,
                    loaders: [
                        'file-loader?hash=sha512&digest=hex&name=images/[hash].[ext]',
                        'image-webpack-loader?bypassOnDebug'
                    ]
                },
                {
                    test: /\.svg$/,
                    use: [
                        'raw-loader'
                    ]
                },
                {
                    test: /\.scss$/,
                    use: [
                        {
                            loader: "style-loader" // creates style nodes from JS strings
                        },
                        {
                            loader: "css-loader" // translates CSS into CommonJS
                        },
                        {
                            loader: "sass-loader" // compiles Sass to CSS
                        }]
                },
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "babel-loader"
                }
            ]
        },
        plugins: [
            new webpack.optimize.CommonsChunkPlugin({
                name: "vendor",
                minChunks: Infinity,
            }),
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                'window.jQuery': 'jquery',
                'window.$': 'jquery',
            }),
            new extractTextPlugin({
                filename: "[name].css",
                allChunks: true
            }),
            new htmlWebpackPlugin({
                chunks: ['vendor','entry'],
                template: "./resources/assets/ejs-templates/main-layout.ejs",
                filename: "../../resources/views/layout/main-layout.blade.php",
                inject: false
            }),
            new htmlWebpackPlugin({
                chunks: ['vendor','auth'],
                template: "./resources/assets/ejs-templates/auth-layout.ejs",
                filename: "../../resources/views/layout/auth-layout.blade.php",
                inject: false
            })
        ]
    }
};

module.exports = config;