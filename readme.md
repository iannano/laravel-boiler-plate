## About Boiler Plate
- CKEditor
- Datatables
- Bootstrap 3.3.7

## Dependency
- "s-ichikawa/laravel-sendgrid-driver": "1.2",
- "yajra/laravel-datatables-oracle": "~7.0",
- "kodeine/laravel-meta": "dev-master"

## Webpack
- "axios": "^0.15.3",
- "babel-cli": "^6.26.0",
- "babel-loader": "^7.1.2",
- "babel-preset-es2015": "^6.24.1",
- "bootstrap-sass": "^3.3.7",
- "cross-env": "^3.2.4",
- "css-loader": "^0.28.7",
- "extract-text-webpack-plugin": "^2.1.2",
- "file-loader": "^0.11.2",
- "html-webpack-plugin": "^2.30.1",
- "image-webpack-loader": "^3.4.2",
- "jquery": "^2.2.4",
- "laravel-mix": "^0.12.1",
- "lodash": "^4.17.4",
- "sass-loader": "^6.0.6",
- "style-loader": "^0.18.2",
- "url-loader": "^0.5.9",
- "vue": "^2.5.2",
- "webpack": "^3.8.1"

## How to Use
Coming Soon

## Structure
Coming Soon

## Circle CI
Coming Soon