<?php

namespace App\Http\Controllers\Helper\Datatables;

use App\Http\Controllers\Factory\DatatablesController;
use App\Http\Repository\Implement\RoleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class Role
 * @package App\Http\Controllers\Helper\Datatables
 */
class Role extends Controller implements DatatablesController
{
    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->roleRepository = new RoleRepository();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function defaultDatatables(Request $request)
    {
        $role = $this->roleRepository->findAll();
        $datatable = Datatables::of($role)
            ->addColumn('action', function ($role) {
                $param = array(
                    "id"    => Crypt::encryptString($role->id)
                );
                return '<a href="'.route("role.update", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a> '.
                    '<a href="'.route("role.update", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> '.
                    '<a href="'.route("role.delete", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Delete</a>';
            })
            ->make(true);
        return $datatable;
    }

}
