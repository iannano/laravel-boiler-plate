<?php

namespace App\Http\Controllers\Helper\Datatables;

use App\Http\Controllers\Factory\DatatablesController;
use App\Http\Repository\Implement\EventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Event extends Controller implements DatatablesController
{
    protected $eventRepository;

    public function __construct()
    {
        $this->eventRepository = new EventRepository();
    }

    public function defaultDatatables(Request $request)
    {
        // TODO: Implement defaultDatatables() method.
    }

}
