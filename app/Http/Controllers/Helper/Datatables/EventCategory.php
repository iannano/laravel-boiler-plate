<?php

namespace App\Http\Controllers\Helper\Datatables;

use App\Http\Controllers\Factory\DatatablesController;
use App\Http\Repository\Implement\EventCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class EventCategory
 * @package App\Http\Controllers\Helper\Datatables
 */
class EventCategory extends Controller implements DatatablesController
{
    /**
     * @var EventCategoryRepository
     */
    protected $eventCategoryRepository;

    /**
     * EventCategory constructor.
     */
    public function __construct()
    {
        $this->eventCategoryRepository = new EventCategoryRepository();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function defaultDatatables(Request $request)
    {
        $eventCategory  = $this->eventCategoryRepository->findAll();
        $datatable      = Datatables::of($eventCategory)
            ->addColumn('action', function ($eventCategory) {
                $param = array(
                    "id"    => Crypt::encryptString($eventCategory->id)
                );
                return '<a href="'.route("event.update", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</a> '.
                    '<a href="'.route("event.update", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> '.
                    '<a href="'.route("event.delete", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->make(true);

        return $datatable;
    }

}
