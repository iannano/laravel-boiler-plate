<?php

namespace App\Http\Controllers\Helper\Datatables;

use App\Http\Controllers\Factory\DatatablesController;
use App\Http\Repository\Implement\NetworkPicRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class NetworkPic
 * @package App\Http\Controllers\Helper\Datatables
 */
class NetworkPic extends Controller implements DatatablesController
{
    /**
     * @var NetworkPicRepository
     */
    protected $networkPicRepository;

    private $networkPicData = [

    ];

    private $networkPicRelation = [

    ];

    /**
     * NetworkPic constructor.
     */
    public function __construct()
    {
        $this->networkPicRepository = new NetworkPicRepository();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function defaultDatatables(Request $request)
    {
        $networkPic = $this->networkPicRepository->findAll($this->networkPicData, $this->networkPicRelation);
        $datatable = Datatables::of($networkPic)
            ->addColumn('action', function ($networkPic) {
                $param = array(
                    "id"    => Crypt::encryptString($networkPic->id)
                );
                return '<a href="'.route("network.pic.update", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a> '.
                    '<a href="'.route("network.pic.update", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> '.
                    '<a href="'.route("network.pic.delete", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Delete</a>';
            })
            ->make(true);
        return $datatable;
    }

}
