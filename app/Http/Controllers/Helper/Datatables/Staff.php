<?php

namespace App\Http\Controllers\Helper\Datatables;

use App\Http\Controllers\Factory\DatatablesController;
use App\Http\Models\StaffModel;
use App\Http\Repository\Implement\CStaff;
use App\Http\Repository\Implement\StaffRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;

/**
 * Class Staff
 * @package App\Http\Controllers\Helper\Datatables
 */
class Staff extends Controller implements DatatablesController
{
    /**
     * @var StaffRepository
     */
    protected $staffRepository;

    /**
     * Staff constructor.
     */
    public function __construct()
    {
        $this->staffRepository = new StaffRepository();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function defaultDatatables(Request $request)
    {
        $staffList = $this->staffRepository->findAll();
        $datatable = Datatables::of($staffList)
            ->addColumn('action', function ($staff) {
                $param = array(
                    "id"    => Crypt::encryptString($staff->id)
                );
                return '<a href="'.route("staff.update", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a> '.
                    '<a href="'.route("staff.update", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> '.
                    '<a href="'.route("staff.delete", $param).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Delete</a>';
            })
            ->make(true);
        return $datatable;
    }

}
