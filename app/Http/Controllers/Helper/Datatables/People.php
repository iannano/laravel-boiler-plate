<?php

namespace App\Http\Controllers\Helper\Datatables;

use App\Http\Controllers\Factory\DatatablesController;
use App\Http\Repository\Implement\PeopleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class People
 * @package App\Http\Controllers\Helper\Datatables
 */
class People extends Controller implements DatatablesController
{
    /**
     * @var PeopleRepository
     */
    protected $peopleRepository;

    private $peopleData = [

    ];

    private $peopleRelation = [
        "networkpic"
    ];

    /**
     * People constructor.
     */
    public function __construct()
    {
        $this->peopleRepository = new PeopleRepository();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function defaultDatatables(Request $request)
    {
        $picSessionLogin = $request->session()->get("picSession");
        $staffSessionLogin = $request->session()->get("staffSession");
        if($staffSessionLogin)
        {
            $people = $this->peopleRepository->findAll($this->peopleData, $this->peopleRelation);
        }

        if($picSessionLogin)
        {
            $param = [
                "network_pic_id"    => $picSessionLogin['id']
            ];
            $people = $this->peopleRepository->findByParam($this->peopleData, $param, $this->peopleRelation);
        }
        $datatable = Datatables::of($people)
            ->addColumn('action', function($people){
                $param = array(
                    "id"    => Crypt::encryptString($people->id)
                );
                return '<a href="'.route("network.people.update", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</a> '.
                    '<a href="'.route("network.people.update", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> '.
                    '<a href="'.route("network.people.delete", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->make(true);

        return $datatable;
    }

}
