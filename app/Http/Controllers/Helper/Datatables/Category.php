<?php

namespace App\Http\Controllers\Helper\Datatables;

use App\Http\Controllers\Factory\DatatablesController;
use App\Http\Repository\Implement\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Facades\Datatables;

class Category extends Controller implements DatatablesController
{
    protected $categoryRepository;

    private $categoryData = [

    ];

    private $categoryRelation = [

    ];

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
    }

    public function defaultDatatables(Request $request)
    {
        $category = $this->categoryRepository->findAll($this->categoryData);
        $datatable = Datatables::of($category)
            ->addColumn('action', function ($category) {
                $param = array(
                    "id"    => Crypt::encryptString($category->id)
                );
                return '<a href="'.route("category.update", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</a> '.
                    '<a href="'.route("category.update", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> '.
                    '<a href="'.route("category.delete", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->make(true);

        return $datatable;
    }

}
