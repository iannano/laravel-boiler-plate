<?php

namespace App\Http\Controllers\Helper\Datatables;

use App\Http\Controllers\Factory\DatatablesController;
use App\Http\Repository\Implement\OrganizationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Facades\Datatables;

/**
 * Class Company
 * @package App\Http\Controllers\Helper\Datatables
 */
class Company extends Controller implements DatatablesController
{
    /**
     * @var OrganizationRepository
     */
    protected $companyRepository;

    /**
     * @var array
     */
    private $companyData = [
        "id",
        "network_pic_id",
        "name",
        "description",
        "created_at"
    ];

    /**
     * @var array
     */
    private $companyRelation = [
        "networkpic"
    ];

    /**
     * Company constructor.
     */
    public function __construct()
    {
        $this->companyRepository = new OrganizationRepository();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function defaultDatatables(Request $request)
    {
        $picSessionLogin = $request->session()->get("picSession");
        $staffSessionLogin = $request->session()->get("staffSession");
        if($staffSessionLogin)
        {
            $company = $this->companyRepository->findAll($this->companyData, $this->companyRelation);
        }

        if($picSessionLogin)
        {
            $param = [
                "network_pic_id" => $picSessionLogin['id']
            ];
            $company = $this->companyRepository->findByParam($this->companyData, $param, $this->companyRelation);
        }
        $datatable = Datatables::of($company)
            ->addColumn('action', function ($company) {
                $param = array(
                    "id"    => Crypt::encryptString($company->id)
                );
                return '<a href="'.route("network.company.update", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> View</a> '.
                    '<a href="'.route("network.company.update", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit</a> '.
                    '<a href="'.route("network.company.delete", $param).'" class="btn btn-xs btn-primary"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->make(true);

        return $datatable;
    }

}
