<?php

namespace App\Http\Controllers\Helper\Upload;

use App\Http\Controllers\Factory\UploadController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class UploadImage
 * @package App\Http\Controllers\Helper\Upload
 */
class UploadImage extends Controller implements UploadController
{
    CONST PUBLIC_PATH = "public/";
    /**
     * UploadImage constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function defaultEndPoint(Request $request)
    {
        $storageDir = $request->input("storage-dir");
        $path = $request
            ->file("file")
            ->store(self::PUBLIC_PATH.$storageDir);

        if($path)
        {
            //Trim public string
            $path = str_replace("public/", "", $path);
            return response()
                ->json($path);
        }
    }
}
