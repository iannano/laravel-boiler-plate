<?php

namespace App\Http\Controllers\Helper\Location;

use App\Http\Repository\Implement\DistrictRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

/**
 * Class District
 * @package App\Http\Controllers\Helper\Location
 */
class District extends Controller
{
    /**
     * @var DistrictRepository
     */
    protected $districtRepository;

    private $select = [
        "id",
        "city_id",
        "district"
    ];

    private $relation = [

    ];

    /**
     * District constructor.
     */
    public function __construct()
    {
        $this->districtRepository = new DistrictRepository();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDistrict(Request $request)
    {
        $cityId = Crypt::decryptString($request->input("cityId"));
        $districtList = $this->districtRepository
            ->findByParam($this->select, array("city_id"=>$cityId), $this->relation, "district","ASC");
        $result = $this->encryptData($districtList);
        return response()->json($result);
    }

    /**
     * @param $districtList
     * @return array
     */
    private function encryptData($districtList)
    {
        $result = array();

        foreach ($districtList as $district)
        {
            $data = array(
                "id"        => Crypt::encryptString($district->id),
                "district"  => $district->district
            );

            array_push($result, $data);
        }

        return $result;
    }
}
