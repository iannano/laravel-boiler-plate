<?php

namespace App\Http\Controllers\Helper\Location;

use App\Http\Repository\Implement\CityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;

/**
 * Class City
 * @package App\Http\Controllers\Helper\Location
 */
class City extends Controller
{
    /**
     * @var CityRepository
     */
    protected $cityRepository;

    /**
     * @var array
     */
    private $select = [
        "id",
        "province_id",
        "city"
    ];

    /**
     * @var array
     */
    private $relation = [

    ];

    /**
     * City constructor.
     */
    public function __construct()
    {
        $this->cityRepository = new CityRepository();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCity(Request $request)
    {
        $province   = Crypt::decryptString($request->input("provinceId"));
        $cityList   = $this->cityRepository
            ->findByParam($this->select, array("province_id"=>$province), $this->relation,"city","ASC");

        $response = $this->encryptData($cityList);

        return response()->json($response);
    }

    /**
     * @param array $cityList
     * @return array
     */
    private function encryptData($cityList)
    {
        $result = array();

        foreach ($cityList as $city)
        {
            $data = array(
                "id"    => Crypt::encryptString($city->id),
                "city"  => $city->city
            );

            array_push($result, $data);
        }

        return $result;
    }
}
