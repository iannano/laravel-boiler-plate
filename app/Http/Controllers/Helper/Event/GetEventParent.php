<?php

namespace App\Http\Controllers\Helper\Event;

use App\Http\Repository\Implement\EventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class GetEventParent
 * @package App\Http\Controllers\Helper\Event
 */
class GetEventParent extends Controller
{
    /**
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * @var array
     */
    protected $eventData = [

    ];

    /**
     * @var array
     */
    protected $eventRelation = [

    ];

    /**
     * GetEventParent constructor.
     */
    public function __construct()
    {
        $this->eventRepository = new EventRepository();
    }

    /**
     * @return $this|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static|static[]
     */
    public function index()
    {
        $param = [
            "parent_id" => NULL
        ];
        return $this->eventRepository->findByParam($this->eventData, $param, $this->eventRelation);
    }
}
