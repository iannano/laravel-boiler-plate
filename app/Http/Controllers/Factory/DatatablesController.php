<?php
/**
 * Created by PhpStorm.
 * User: ian-nano
 * Date: 10/18/17
 * Time: 07:15
 */

namespace App\Http\Controllers\Factory;


use Illuminate\Http\Request;

/**
 * Interface DatatablesController
 * @package App\Http\Controllers\Factory
 */
interface DatatablesController
{
    /**
     * DatatablesController constructor.
     */
    public function __construct();

    /**
     * Use for default datatables logic
     * @param Request $request
     * @return mixed
     */
    public function defaultDatatables(Request $request);
}