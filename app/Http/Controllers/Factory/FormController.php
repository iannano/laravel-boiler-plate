<?php

namespace App\Http\Controllers\Factory;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Interface FormController
 * @package App\Http\Controllers\Factory
 */
interface FormController
{
    /**
     * FormController constructor.
     */
    public function __construct();

    /**
     * Use for show form
     * @param Request $request
     * @return mixed
     */
    public function view(Request $request);

    /**
     * Use for manage business logic after form submit
     * @param Request $request
     * @return mixed
     */
    public function submit(Request $request);
}
