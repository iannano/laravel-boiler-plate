<?php
/**
 * Created by PhpStorm.
 * User: ian-nano
 * Date: 10/24/17
 * Time: 13:48
 */

namespace App\Http\Controllers\Factory;
use Illuminate\Http\Request;


/**
 * Interface UploadController
 * @package App\Http\Controllers\Factory
 */
interface UploadController
{
    /**
     * UploadController constructor.
     */
    public function __construct();

    /**
     * @param Request $request
     * @return mixed
     */
    public function defaultEndPoint(Request $request);
}