<?php

namespace App\Http\Controllers\Factory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Interface ViewController
 * @package App\Http\Controllers\Factory
 */
interface ViewController
{
    /**
     * ViewController constructor.
     */
    public function __construct();

    /**
     * Show UI / UX
     * @param Request $request
     * @return mixed
     */
    public function view(Request $request);
}
