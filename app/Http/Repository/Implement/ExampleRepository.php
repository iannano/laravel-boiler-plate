<?php
/**
 * Created by PhpStorm.
 * User: ian-nano
 * Date: 30/12/17
 * Time: 00.17
 */

namespace App\Http\Repository\Implement;


use App\Http\Models\ExampleModel;
use App\Http\Repository\Contract\QueryBaseFactory;

class ExampleRepository extends QueryBaseFactory
{
    protected $model;

    protected $primaryKey;

    /**
     * EventRepository constructor.
     */
    public function __construct()
    {
        $this->model = new ExampleModel();
        $this->primaryKey = $this->model->getKeyName();
    }
}