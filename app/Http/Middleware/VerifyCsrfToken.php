<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        "helper/datatables/staff-default",
        "helper/datatables/network-pic-default",
        "helper/datatables/network-people-default",
        "helper/datatables/network-company-default",
        "helper/datatables/role-default",
        "helper/datatables/category-default"
    ];
}
