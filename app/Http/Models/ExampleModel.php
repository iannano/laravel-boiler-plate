<?php
/**
 * Created by PhpStorm.
 * User: ian-nano
 * Date: 30/12/17
 * Time: 00.18
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class ExampleModel extends Model
{
    protected $table = "Table Name";
    protected $primaryKey = "Primary Key";
}