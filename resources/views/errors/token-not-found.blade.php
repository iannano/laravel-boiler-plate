<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

    <style>
        a, a:visited{
            text-decoration: none !important;
            color: black;
        }
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }
        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }
        .content {
            text-align: center;
            display: inline-block;
            color: black;
        }
        .title {
            font-size: 72px;
            margin-bottom: 40px;
            color: black;
        }
        .sub-title {
            font-size: 52px;
            margin-bottom: 40px;
            color: black;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Token Not Found</div>
        <div class="sub-title">
            <a href="{{ route('auth.signin') }}"> >> Back to signin << </a>
        </div>
        <div class="content">
            Please contact <a href="mailto:submission@kosmik.id">submission@kosmik.id</a> if something not right
        </div>
    </div>
</div>
</body>
</html>