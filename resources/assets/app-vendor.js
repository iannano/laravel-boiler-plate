window.baseUrl      = window.location.origin+"/popconasia2018/public/";
window.storageUrl   = window.location.origin+"/popconasia2018/public/storage/";
/**
 * Core CSS
 */
require ("./plugins/bootstrap-3.3.7-dist 2/css/bootstrap.css");
require ("./plugins/AdminLTE/css/AdminLTE.css");
require ("./plugins/AdminLTE/css/skins/_all-skins.css");
require ("./plugins/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css");

/**
 * Core JS
 */
require ("jquery");
require ("./plugins/bootstrap-3.3.7-dist 2/js/bootstrap");
require ("./plugins/AdminLTE/js/adminlte");
require ("./plugins/AdminLTE/js/app");
require ("./plugins/DataTables/DataTables-1.10.16/js/dataTables.bootstrap");
