"use strict";
import axios from "axios";

const uploadForm        = document.getElementById("upload-form");
let fileUpload          = document.getElementById("file");
let resetUpload         = document.getElementById("re-upload");

//Template upload
const upload = () =>{
    return '<label for="picture">Picture / Logo</label>\n' +
        '<input type="file" data-store="people" class="form-control" name="file" id="file"/>';
};

//Template result
const result = () =>{
    return '<label for="result" class="col-sm-3">Image Result</label>\n' +
        '<div class="col-sm-9"><img src="#" id="image-placeholder" class="img-responsive" style="padding-bottom: 1em"/>\n' +
        '<input type="hidden" name="picture" id="path"/>\n' +
        '<button type="button" id="re-upload" class="btn btn-danger">Change Image</button></div>';
};

//Upload to server
const uploadToServer = () =>{
    let fileUpload  = document.getElementById("file");
    let file        = fileUpload.files[0];
    if(file)
    {
        let formData = new FormData();
        formData.append("file", fileUpload.files[0]);
        formData.append("storage-dir", fileUpload.getAttribute("data-store"));
        return axios({
            url: window.baseUrl+"helper/upload/image",
            method: "POST",
            headers:{
                "X-Requested-With": "XMLHttpRequest",
                "Content-Type": "multipart/form-data"
            },
            data: formData
        });
    }
};

//TODO: Change rootURL if want to upload data
//Render result from server response
const renderResult = (responseFromUpload) =>{
    //Template Rendering
    uploadForm.innerHTML = "";
    uploadForm.innerHTML = result();

    //Get Template ID
    const reUpload          = document.getElementById("re-upload");
    const path              = document.getElementById("path");
    const imagePlaceHolder  = document.getElementById("image-placeholder");

    path.value = responseFromUpload;
    imagePlaceHolder.setAttribute("src",window.storageUrl+responseFromUpload);

    reUpload.addEventListener("click", ()=>{
        uploadForm.innerHTML = "";
        uploadForm.innerHTML = upload();

        entryUpload();
    });
};

//Trigger change to upload
const entryUpload = () =>{
    let fileUpload        = document.getElementById("file");

    fileUpload.addEventListener("change", () =>{
        uploadToServer().then((response)=>{
            if(response.data !== null)
            {
                renderResult(response.data);
            }
        }).catch((error)=>{
            throw new Error(error);
        })
    });
};

if(!!fileUpload)
{
    entryUpload();
}

if(!!resetUpload)
{
    resetUpload.addEventListener("click", ()=>{
        uploadForm.innerHTML = "";
        uploadForm.innerHTML = upload();

        entryUpload();
    });
}