"use strict";
import axios from "axios";

//Dom Selection
const PROVINCE  = document.getElementById("province");
let CITY        = document.getElementById("city");
const DISTRICT  = document.getElementById("district");

//End Point
const END_POINT_CITY    = "helper/location/get-city";
const END_POINT_DISTRICT= "helper/location/get-district";

/**
 * Request Data From Server
 * @param urlSearchParam
 * @param endPoint
 */
const requestData = (urlSearchParam, endPoint) =>{
    return axios({
        url: window.baseUrl+endPoint,
        method: "POST",
        headers:{
            "X-Requested-With": "XMLHttpRequest"
        },
        data: urlSearchParam
    });
};

/**
 * Append data to select DOM
 * @param dataList
 * @param DOM
 */
const appendData = (dataList, DOM) =>{
    if(DOM === "city")
    {
        dataList.forEach((data)=>{
            let optionAttribute = document.createElement("option");
            optionAttribute.value   = data.id;
            optionAttribute.text    = data.city;
            CITY.appendChild(optionAttribute);
        });
    }

    if(DOM === "district")
    {
        if(!!DISTRICT)
        {
            dataList.forEach((data)=>{
                let optionAttribute = document.createElement("option");
                optionAttribute.value   = data.id;
                optionAttribute.text    = data.district;
                DISTRICT.appendChild(optionAttribute);
            });
        }
    }
};

/**
 * Reset Data in select DOM
 * @param DOM
 */
const resetData = (DOM) =>{
    if(DOM === "city")
    {
        CITY.options.length = 0;
        let optionAttribute = document.createElement("option");
        optionAttribute.value   = "";
        optionAttribute.text    = " -- Select City -- ";
        CITY.appendChild(optionAttribute);
    }

    if(DOM === "district")
    {
        if(!!DISTRICT)
        {
            DISTRICT.options.length = 0;
            let optionAttribute     = document.createElement("option");
            optionAttribute.value   = "";
            optionAttribute.text    = " -- Select District -- ";
            DISTRICT.appendChild(optionAttribute);
        }
    }
};

/**
 * Event listener province
 */
if(!!PROVINCE)
{
    PROVINCE.addEventListener("change",()=>{
        resetData("city");
        resetData("district");

        let urlSearchParam = new URLSearchParams();
        urlSearchParam.append("provinceId", PROVINCE.value);
        requestData(urlSearchParam, END_POINT_CITY).then((response)=>{
            appendData(response.data, "city");
        }).catch((error)=>{
            throw new Error("Exception: "+error);
        });
    });
}

/**
 * Event listener city
 */
if(!!CITY)
{
    CITY.addEventListener("change",()=>{
        resetData("district");

        let urlSearchParam = new URLSearchParams();
        urlSearchParam.append("cityId", CITY.value);
        requestData(urlSearchParam, END_POINT_DISTRICT).then((response)=>{
            appendData(response.data, "district");
        }).catch((error)=>{
            throw new Error("Exception: "+error);
        });
    });
}