"use strict";
const tableCategory = $("#category-table");
if(!!tableCategory)
{
    tableCategory.dataTable({
        serverSide: true,
        ajax:{
            "url": window.baseUrl+"helper/datatables/category-default",
            "method": "POST"
        },
        columns: [
            {
                data: 'id',
                name: 'id',
            },
            {
                data: 'category',
                name: 'category'
            },
            {
                data: 'type',
                name: 'type'
            },
            {
                data: 'created_at',
                name: 'created_at',
            },
            {
                data: 'updated_at',
                name: 'updated_at'
            },
            {
                data: 'action',
                name: 'action'
            }
        ],
        order: [[0, 'asc']]
    })
}