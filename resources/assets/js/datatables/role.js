"use strict";
const tableRole = $("#role-table");
if(!!tableRole)
{
    tableRole.dataTable({
        serverSide: true,
        ajax:{
            "url": window.baseUrl+"helper/datatables/role-default",
            "method": "POST"
        },
        columns: [
            {
                data: 'id',
                name: 'id',
            },
            {
                data: 'role',
                name: 'role'
            },
            {
                data: 'created_at',
                name: 'created_at',
            },
            {
                data: 'updated_at',
                name: 'updated_at'
            },
            {
                data: 'action',
                name: 'action'
            }
        ],
        order: [[0, 'asc']]
    })
}