"use strict";
const EVENT_CATEGORY = document.getElementById("eventCategoryTable");
if(!!EVENT_CATEGORY)
{
    EVENT_CATEGORY.dataTable({
        processing: true,
        serverSide: true,
        ajax:{
            "url": window.baseUrl+"helper/datatables/network-company-default",
            "method": "POST"
        },
        columns: [
            {
                data: 'id',
                name: 'id',
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'networkpic.fullname',
                name: 'networkpic.fullname',
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'action',
                name: 'action'
            }
        ],
        order: [[0, 'asc']]
    });
}