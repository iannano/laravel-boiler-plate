"use strict";
const tableNetworkPeople = $("#network-people-table");
if(!!tableNetworkPeople)
{
    tableNetworkPeople.DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            "url": window.baseUrl+"helper/datatables/network-people-default",
            "method": "POST"
        },
        columns: [
            {
                data: 'id',
                name: 'id',
            },
            {
                data: 'fullname',
                name: 'fullname'
            },
            {
                data: 'networkpic.fullname',
                name: 'networkpic.fullname',
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'updated_at',
                name: 'updated_at'
            },
            {
                data: 'action',
                name: 'action'
            }
        ],
        order: [[0, 'asc']]
    });
}