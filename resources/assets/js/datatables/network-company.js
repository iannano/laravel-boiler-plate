"use strict";
const tableNetworkCompany = $("#network-company-table");
if(!!tableNetworkCompany)
{
    tableNetworkCompany.DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            "url": window.baseUrl+"helper/datatables/event-category-default",
            "method": "POST"
        },
        columns: [
            {
                data: 'id',
                name: 'id',
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'networkpic.fullname',
                name: 'networkpic.fullname',
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'action',
                name: 'action'
            }
        ],
        order: [[0, 'asc']]
    });
}