"use strict";
let tableStaff = $("#staff-table");
if(!!tableStaff)
{
    tableStaff.DataTable({
        processing: true,
        serverSide: true,
        ajax:{
            "url": window.baseUrl+"helper/datatables/staff-default",
            "method": "POST"
        },
        columns: [
            {
                data: 'id',
                name: 'id',
            },
            {
                data: 'fullname',
                name: 'fullname'
            },
            {
                data: 'username',
                name: 'username',
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'status',
                name: 'status',
                render: (data, type, row) =>{
                    if(parseInt(data) === 1)
                    {
                        return "<span class='label label-primary'>Active</span>";
                    }
                    if(parseInt(data) === 0)
                    {
                        return "<span class='label label-warning'>Not Active</span>";
                    }
                    if(parseInt(data) === 2)
                    {
                        return "<span class='label label-danger'>Banned</span>"
                    }
                }
            },
            {
                data: 'action',
                name: 'action'
            }
        ],
        order: [[0, 'asc']]
    });
}