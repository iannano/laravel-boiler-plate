"use strict";
const TEXTAREA = document.getElementsByClassName("CKEditor");
if(!!TEXTAREA)
{
    let totalTextArea = TEXTAREA.length;
    for (let i = 0; i < totalTextArea; i++)
    {
        CKEDITOR.replace(TEXTAREA[i]);
    }
}