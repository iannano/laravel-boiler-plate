/**
 * Custom CSS
 */
require ("./css/ckeditor5.css");
require ("./css/additional.css");

/**
 * Datatables Entry
 */
require ("./js/datatables/staff");
require ("./js/datatables/network-pic");
require ("./js/datatables/network-people");
require ("./js/datatables/network-company");
require ("./js/datatables/role");
require ("./js/datatables/category");

/**
 * Location Entry
 */
require ("./js/location");

/**
 * CKEditor
 */
require ("./js/ckeditor/init");

/**
 * Upload File
 */
require ("./js/upload");

/**
 * Single Event Listener
 */
require ("./js/single-event");