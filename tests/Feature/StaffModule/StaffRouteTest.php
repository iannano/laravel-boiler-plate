<?php

namespace Tests\Feature\StaffModule;

use App\Http\Controllers\Staff\Create;
use Illuminate\Support\Facades\Crypt;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StaffRouteTest extends TestCase
{
    /**
     * @group Routing Staff
     * @covers Get::view()
     */
    public function testAllowAccessStaffIndexWithGetMethod()
    {
        $response = $this->get("staff");
        $response->assertStatus(200);
    }

    /**
     * @group Routing Staff
     * @covers Get::view()
     */
    public function testPreventAccessStaffIndexWithPostMethod()
    {
        $response = $this->post("staff");
        $response->assertStatus(405);
    }

    /**
     * @group Routing Staff
     * @covers Create::view()
     */
    public function testAllowAccessStaffCreateWithGetMethod()
    {
        $response = $this->get("staff/form-create");
        $response->assertStatus(200);
    }

    /**
     * @group Routing Staff
     * @covers Create::view()
     */
    public function testPreventAccessStaffCreateWithPostMethod()
    {
        $response = $this->post("staff/form-create");
        $response->assertStatus(405);
    }

    /**
     * @group Routing Staff
     * @covers Create::view()
     */
    public function testAllowAccessStaffUpdateWithGetMethod()
    {
        $response = $this->get("staff/form-update?id=".Crypt::encryptString(1));
        $response->assertStatus(200);
    }

    public function testPreventAccessStaffUpdateWithPostMethod()
    {
        $response = $this->post("staff/form-update?id=".Crypt::encryptString(1));
        $response->assertStatus(405);
    }
}
