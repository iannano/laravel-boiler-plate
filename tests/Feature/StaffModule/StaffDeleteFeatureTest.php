<?php

namespace Tests\Feature\StaffModule;

use App\Http\Controllers\Staff\Delete;
use Illuminate\Support\Facades\Crypt;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StaffDeleteFeatureTest extends TestCase
{
    /**
     * @group Delete Staff
     * @covers Delete::submit()
     */
    public function testAllowDeleteStaffRecordWithoutEmptyId()
    {
        $data = array(
            "id"    => Crypt::encryptString(1)
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-delete/submit", $data, $header);
        $response->assertStatus(302)
            ->isRedirect("staff");
    }
}
