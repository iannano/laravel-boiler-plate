<?php

namespace Tests\Feature\StaffModule;

use App\Http\Controllers\Staff\Update;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StaffUpdateFeatureTest extends TestCase
{

    /**
     * @group Update Staff
     * @covers Update::submit()
     */
    public function testAllowedUpdateStaffRecordWithoutEmptyData()
    {
        $dataInput = array(
            "id"        => "1",
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "Ian",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff");
    }

    public function testPreventUpdateStaffRecordWithEmptyId()
    {
        $dataInput = array(
            "id"        => "",
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "Ian",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-update");
    }

    public function testPreventUpdateStaffRecordWithEmptyRoleId()
    {
        $dataInput = array(
            "id"        => "1",
            "role_id"   => "",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "Ian",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-update");
    }

    public function testPreventUpdateStaffRecordWithEmptyFullname()
    {
        $dataInput = array(
            "id"        => "1",
            "role_id"   => "1",
            "fullname"  => "",
            "username"  => "Ian",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-update");
    }

    public function testPreventUpdateStaffRecordWithEmptyUsername()
    {
        $dataInput = array(
            "id"        => "1",
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-update");
    }

    public function testPreventUpdateStaffRecordWithEmptyEmail()
    {
        $dataInput = array(
            "id"        => "1",
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "Ian",
            "email"     => "",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-update");
    }

    public function testPreventUpdateStaffRecordWithEmptyPassword()
    {
        $dataInput = array(
            "id"        => "1",
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "Ian",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-update");
    }

    public function testPreventUpdateStaffRecordWithDifferentPasswordAndRePassword()
    {
        $dataInput = array(
            "id"            => "1",
            "role_id"       => "1",
            "fullname"      => "Ibnu Aulia Nugraha A",
            "username"      => "Ian",
            "email"         => "ibnuauliana@gmail.com",
            "password"      => "ianian100994",
            "re-password"   => "100994ianian",
            "status"        => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-update");
    }

    public function testPreventUpdateStaffRecordWithEmptyStatus()
    {
        $dataInput = array(
            "id"            => "1",
            "role_id"       => "1",
            "fullname"      => "Ibnu Aulia Nugraha A",
            "username"      => "Ian",
            "email"         => "ibnuauliana@gmail.com",
            "password"      => "ianian100994",
            "re-password"   => "100994ianian",
            "status"        => ""
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-update");
    }

    public function testPreventUpdateStaffRecordWithEmptyCSRFToken()
    {
        $dataInput = array(
            "id"            => "1",
            "role_id"       => "1",
            "fullname"      => "Ibnu Aulia Nugraha A",
            "username"      => "Ian",
            "email"         => "ibnuauliana@gmail.com",
            "password"      => "ianian100994",
            "re-password"   => "100994ianian",
            "status"        => 0
        );

        $header = array(
            "_token"    => ""
        );

        $response = $this->post("staff/form-update/submit", $dataInput, $header);
        $response->assertStatus(302);
    }
}
