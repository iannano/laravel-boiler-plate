<?php

namespace Tests\Feature\StaffModule;

use App\Http\Controllers\Staff\Create;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class StaffCreateFeatureTest
 * @package Tests\Feature\StaffModule
 */
class StaffCreateFeatureTest extends TestCase
{
    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testAllowedCreateNewStaffWithoutEmptyData()
    {
        $dataInput = array(
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "Ian",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff");
    }

    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testPreventCreateNewStaffWithEmptyRoleId()
    {
        $dataInput = array(
            "role_id"   => "",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "Ian",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-create");
    }

    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testPreventCreateNewStaffWithEmptyFullname()
    {
        $dataInput = array(
            "role_id"   => "1",
            "fullname"  => "",
            "username"  => "Ian",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-create");
    }

    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testPreventCreateNewStaffWithEmptyUsername()
    {
        $dataInput = array(
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-create");
    }

    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testPreventCreateNewStaffWithEmptyEmail()
    {
        $dataInput = array(
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "ianian100994",
            "email"     => "",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-create");
    }

    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testPreventCreateNewStaffWithWrongFormatEmail()
    {
        $dataInput = array(
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "ianian100994",
            "email"     => "inibukanformatemail",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-create");
    }

    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testPreventCreateNewStaffWithEmptyPassword()
    {
        $dataInput = array(
            "role_id"   => "1",
            "fullname"  => "Ibnu Aulia Nugraha A",
            "username"  => "ianian100994",
            "email"     => "ibnuauliana@gmail.com",
            "password"  => "ianian100994",
            "status"    => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-create");
    }

    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testPreventCreateNewStaffWithDifferentPasswordAndRePassword()
    {
        $dataInput = array(
            "role_id"       => "1",
            "fullname"      => "Ibnu Aulia Nugraha A",
            "username"      => "ianian100994",
            "email"         => "ibnuauliana@gmail.com",
            "password"      => "ianian100994",
            "re-password"   => "100994ianian",
            "status"        => 0
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-create");
    }

    /**
     * @group Create Staff Feature
     * @covers Create::submit()
     */
    public function testPreventCreateNewStaffWithEmptyStatus()
    {
        $dataInput = array(
            "role_id"       => "1",
            "fullname"      => "Ibnu Aulia Nugraha A",
            "username"      => "ianian100994",
            "email"         => "ibnuauliana@gmail.com",
            "password"      => "ianian100994",
            "re-password"   => "100994ianian",
            "status"        => ""
        );

        $header = array(
            "_token"    => csrf_token()
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302)
            ->isRedirect("staff/form-create");
    }

    /**
     *
     */
    public function testPreventCreateNewStaffWithoutCSRFToken()
    {
        $dataInput = array(
            "role_id"       => "1",
            "fullname"      => "Ibnu Aulia Nugraha A",
            "username"      => "ianian100994",
            "email"         => "ibnuauliana@gmail.com",
            "password"      => "ianian100994",
            "re-password"   => "100994ianian",
            "status"        => 1
        );

        $header = array(
            "_token"    => ""
        );

        $response = $this->post("staff/form-create/submit", $dataInput, $header);
        $response->assertStatus(302);
    }
}
