<?php

namespace Tests\Unit\StaffModule;

use App\Http\Controllers\Staff\Update;
use App\Http\Repository\Implement\StaffRepository;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CheckHashPasswordTest extends TestCase
{
    /**
     * @group Check Hash Password
     * @covers Update::checkHashPassword()
     */
    public function testIfPasswordHasSameHashValue()
    {
        $updateClass        = new Update();
        $staffRepository    = new StaffRepository();
        $newPassword        = "ianian100994";
        $staffData          = $staffRepository->findById("1");

        $reflection = new \ReflectionClass(get_class($updateClass));
        $method = $reflection->getMethod("checkHashPassword");
        $method->setAccessible(true);
        $result = $method->invokeArgs($updateClass, array(
            $newPassword, $staffData->password
        ));
        $this->assertEquals("0",$result);
    }

    /**
     * @group Check Hash Password
     * @covers Update::checkHashPassword()
     */
    public function testIfPasswordHasDifferentHashValue()
    {
        $newPassword        = "011213";
        $staffRepository    = new StaffRepository();
        $staffData          = $staffRepository->findById("1");
        $updateClass        = new Update();

        $reflection = new \ReflectionClass(get_class($updateClass));
        $method = $reflection->getMethod("checkHashPassword");
        $method->setAccessible(true);
        $result = $method->invokeArgs($updateClass, array(
            $newPassword, $staffData->password
        ));

        $this->assertEquals("1",$result);
    }
}
