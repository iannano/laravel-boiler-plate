s\Feature\StaffModule\StaffCreateFeature
 [x] Allowed create new staff without empty data
 [x] Prevent create new staff with empty role id
 [x] Prevent create new staff with empty fullname
 [x] Prevent create new staff with empty username
 [x] Prevent create new staff with empty email
 [x] Prevent create new staff with wrong format email
 [x] Prevent create new staff with empty password
 [x] Prevent create new staff with different password and re password
 [x] Prevent create new staff with empty status
 [x] Prevent create new staff without c s r f token

s\Feature\StaffModule\StaffDeleteFeature
 [x] Allow delete staff record without empty id

s\Feature\StaffModule\StaffRoute
 [x] Allow access staff index with get method
 [x] Prevent access staff index with post method
 [x] Allow access staff create with get method
 [x] Prevent access staff create with post method
 [x] Allow access staff update with get method
 [x] Prevent access staff update with post method

s\Feature\StaffModule\StaffUpdateFeature
 [x] Allowed update staff record without empty data
 [x] Prevent update staff record with empty id
 [x] Prevent update staff record with empty role id
 [x] Prevent update staff record with empty fullname
 [x] Prevent update staff record with empty username
 [x] Prevent update staff record with empty email
 [x] Prevent update staff record with empty password
 [x] Prevent update staff record with different password and re password
 [x] Prevent update staff record with empty status
 [x] Prevent update staff record with empty c s r f token

s\Unit\StaffModule\CheckHashPassword
 [x] If password has same hash value
 [x] If password has different hash value

